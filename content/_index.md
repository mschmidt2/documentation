---
title: CentOS Stream Kernel Documentation
---

Welcome to documentation about development of Red Hat-based Linux kernel projects

: [CentOS stream 9 kernel](https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9)

: [Fedora/ARK kernel](https://gitlab.com/cki-project/kernel-ark)

: [Private RHEL kernels](https://gitlab.com/redhat/rhel/src/kernel)


Main documents

: [Quick start guide](docs/readme.html)

: [RHEL kernel workflow](docs/rhel_kernel_workflow.html)
